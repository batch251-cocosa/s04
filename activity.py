class Animal():
	def __init__(self):
		self._sound = "*quiet*"
		
	def eat(self,food):
		pass


	def make_sound(self):
		print(f"{self._sound}")


class Cat(Animal):
	def __init__(self, name, breed, age):
		super().__init__()
		self._name = name
		self._breed = breed
		self._age = age

	def set_name(self, name):
		self._name = name

	def get_name(self):
		print(f"Cat's name is {self._name}")

	def set_sound(self, sound = None):
		if sound is None:
			self._sound = "Meow, Meooooooow"
		else:
			self._sound = sound

	def eat(self, food):
		print(f"{self._name} happily ate the {food}")

	def call(self):
		print(f"Swswswswsw! {self._name}! Come here!")


class Dog(Animal):
	def __init__(self, name, breed, age):
		super().__init__()
		self._name = name
		self._breed = breed
		self._age = age

	def set_name(self, name):
		self._name = name

	def get_name(self):
		print(f"Good boy's name is {self._name}")

	def set_sound(self, sound = None):
		if sound is None:
			self._sound = "Awooooooo!"
		else:
			self._sound = sound

	def eat(self, food):
		print(f"{self._name} gorges the {food}")

	def call(self):
		print(f"Choooooo! {self._name}!")


dog1 = Dog("Popo", "Dalmatian", 15)
dog1.eat("Steak")
dog1.make_sound()
dog1.set_sound()
dog1.make_sound()
dog1.set_sound("Arf! Arf! Arf!")
dog1.make_sound()
dog1.call()


cat1 = Cat("Puss", "Persian", 4)
cat1.eat("Tuna")
cat1.make_sound()
cat1.set_sound()
cat1.make_sound()
cat1.set_sound("Nyaaaaaaaaaaw!")
cat1.make_sound()
cat1.call()